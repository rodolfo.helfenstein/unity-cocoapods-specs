Pod::Spec.new do |spec|
  spec.name         = "Unity"
  spec.version      = "3.0.5"
  spec.summary      = "iOS & Android Shared Library in Kotlin/Native for Rick & Morty API"
  spec.homepage     = "https://github.com/rodolfohelfenstein/unity.git"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "Rodolfo Helfenstein Bulgam" => "rodolfo.helfenstein@dextra-sw.com" }
  spec.source       = { :git => "https://github.com/rodolfohelfenstein/unity.git", :tag => "#{spec.version}" }
  spec.module_name  = "Unity"

  spec.ios.deployment_target = '9.0'
  spec.vendored_frameworks = "build/dist/#{spec.name}.framework"

  spec.prepare_command = <<-CMD
    set -ev
    gradle wrapper
    bundle install
    bundle exec fastlane createFramework release:true
  CMD

end
